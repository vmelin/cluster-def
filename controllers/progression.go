package controllers

import (
	"context"
	"fmt"
	"time"

	gitopsv1 "gitlab.com/Orange-OpenSource/kanod/cluster-def/api/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/rest"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

const (
	ANNOTATION = "clusterdef.kanod.io/progression"
	TIMEOUT    = 3600
)

func (r *ClusterDefReconciler) getProgressionWithClient(
	ctx context.Context,
	nsName types.NamespacedName) (TransientState, int64, error) {

	now := time.Now().Unix()

	restConfigLocal, _ := rest.InClusterConfig()
	dynClientLocal, err := dynamic.NewForConfig(restConfigLocal)
	if err != nil {
		r.Log.Error(err, "Problem while creating client for local cluster.")
		return TSNotPivoted, now, err
	}

	gvrclusterdef := schema.GroupVersionResource{
		Group:    "gitops.kanod.io",
		Version:  "v1alpha1",
		Resource: "clusterdefs",
	}

	clusterdefCR, err := dynClientLocal.Resource(gvrclusterdef).Namespace(nsName.Namespace).Get(ctx, nsName.Name, metav1.GetOptions{})

	if err != nil {
		r.Log.Error(err, "Cannot get clusterdef custom resource.")
		return TSNotPivoted, now, err
	}

	annot := clusterdefCR.GetAnnotations()
	annotStep, ok := annot[ANNOTATION]
	if ok {
		var step TransientState
		var recDate int64
		fmt.Sscanf(annotStep, "%d/%d", &step, &recDate)
		return step, recDate, nil
	} else {
		return TSNotPivoted, now, nil
	}
}

func (r *ClusterDefReconciler) setProgression(ctx context.Context, cdef *gitopsv1.ClusterDef, step TransientState) error {
	now := time.Now().Unix()
	patch := fmt.Sprintf(`{"metadata":{"annotations":{"%s": "%d/%d"}}}`, ANNOTATION, step, now)
	err := r.Client.Patch(
		ctx,
		&gitopsv1.ClusterDef{
			ObjectMeta: metav1.ObjectMeta{
				Name:      cdef.Name,
				Namespace: cdef.Namespace,
			},
		},
		client.RawPatch(types.MergePatchType, []byte(patch)),
	)
	if err != nil {
		r.Log.Error(err, "Cannot patch clusterdef configuration.")
	} else {
		r.Log.Info("Phase completed", "next step", step)
	}

	return err
}

func (r *ClusterDefReconciler) setAnnotation(
	ctx context.Context,
	cdef *gitopsv1.ClusterDef,
	annotation string,
	value string,
) error {
	patch := fmt.Sprintf(`{"metadata":{"annotations":{"%s": "%s"}}}`, annotation, value)
	err := r.Client.Patch(
		ctx,
		&gitopsv1.ClusterDef{
			ObjectMeta: metav1.ObjectMeta{
				Name:      cdef.Name,
				Namespace: cdef.Namespace,
			},
		},
		client.RawPatch(types.MergePatchType, []byte(patch)),
	)
	if err != nil {
		r.Log.Error(err, "Cannot patch clusterdef configuration.")
	} else {
		r.Log.Info("Add annotation", "annotation", annotation, "value", value)
	}
	return err
}
