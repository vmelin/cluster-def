/*
Copyright 2022 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"encoding/json"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	clientapi "k8s.io/client-go/tools/clientcmd/api"
	capi "sigs.k8s.io/cluster-api/api/v1beta1"
)

// return a restClient for capi CRD
func getClientForCapi(kubeconfig *clientapi.Config, grpVersion *schema.GroupVersion) *rest.RESTClient {
	clientConfig := clientcmd.NewDefaultClientConfig(
		*kubeconfig,
		&clientcmd.ConfigOverrides{},
	)
	restConfig, _ := clientConfig.ClientConfig()

	restConfig.ContentConfig.GroupVersion = grpVersion //&capi.GroupVersion
	restConfig.APIPath = "/apis"
	capi.AddToScheme(clientgoscheme.Scheme)
	restConfig.NegotiatedSerializer = serializer.NewCodecFactory(clientgoscheme.Scheme)
	restConfig.UserAgent = rest.DefaultKubernetesUserAgent()
	restClient, _ := rest.UnversionedRESTClientFor(restConfig)

	return restClient
}

// generate a kubeconfig from tlsClientConfig in ArgoCD secret
func createKubeconfigFromArgoSecret(name string, secret corev1.Secret) (*clientapi.Config, error) {
	type TLSClientConfig struct {
		Insecure bool   `json:"insecure"`
		CertData []byte `json:"certData"`
		KeyData  []byte `json:"keyData"`
		CAData   []byte `json:"caData"`
	}

	type TLS struct {
		TlsClientConfig TLSClientConfig `json:"tlsClientConfig"`
	}

	var tls TLS = TLS{}

	err := json.Unmarshal(secret.Data["config"], &tls)
	if err != nil {
		return nil, err
	}

	clusterName := name
	userName := "user"
	contextName := "context"

	configArgo := clientapi.NewConfig()
	credentials := clientapi.NewAuthInfo()
	credentials.ClientCertificate = ""
	credentials.ClientCertificateData = tls.TlsClientConfig.CertData
	credentials.ClientKey = ""
	credentials.ClientKeyData = tls.TlsClientConfig.KeyData
	configArgo.AuthInfos[userName] = credentials

	cluster := clientapi.NewCluster()
	cluster.Server = (string)(secret.Data["server"])
	cluster.CertificateAuthority = ""
	cluster.CertificateAuthorityData = tls.TlsClientConfig.CAData
	cluster.InsecureSkipTLSVerify = false
	configArgo.Clusters[clusterName] = cluster

	context2 := clientapi.NewContext()
	context2.Cluster = clusterName
	context2.AuthInfo = userName
	configArgo.Contexts[contextName] = context2
	configArgo.CurrentContext = contextName
	return configArgo, nil
}

// generate a kubeconfig file from rest.Config
func generateKubeconfigFromRestconfig(restconf *rest.Config) *clientapi.Config {
	clusterName := "cluster"
	userName := "user"
	contextName := "context"

	config := clientapi.NewConfig()
	credentials := clientapi.NewAuthInfo()
	credentials.Token = restconf.BearerToken
	credentials.ClientCertificate = restconf.TLSClientConfig.CertFile
	if len(credentials.ClientCertificate) == 0 {
		credentials.ClientCertificateData = restconf.TLSClientConfig.CertData
	}
	credentials.ClientKey = restconf.TLSClientConfig.KeyFile
	if len(credentials.ClientKey) == 0 {
		credentials.ClientKeyData = restconf.TLSClientConfig.KeyData
	}
	config.AuthInfos[userName] = credentials

	cluster := clientapi.NewCluster()
	cluster.Server = restconf.Host
	cluster.CertificateAuthority = restconf.CAFile
	if len(cluster.CertificateAuthority) == 0 {
		cluster.CertificateAuthorityData = restconf.CAData
	}
	cluster.InsecureSkipTLSVerify = restconf.Insecure
	config.Clusters[clusterName] = cluster

	context2 := clientapi.NewContext()
	context2.Cluster = clusterName
	context2.AuthInfo = userName
	config.Contexts[contextName] = context2
	config.CurrentContext = contextName

	return config
}
