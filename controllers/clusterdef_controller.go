/*
Copyright 2020-22 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"
	"time"

	"github.com/go-logr/logr"
	argocd "gitlab.com/Orange-OpenSource/kanod/cluster-def/thirdparty/argocd/apis/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	clientgo "k8s.io/client-go/kubernetes"
	clientapi "k8s.io/client-go/tools/clientcmd/api"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/handler"
	"sigs.k8s.io/controller-runtime/pkg/source"

	gitopsv1 "gitlab.com/Orange-OpenSource/kanod/cluster-def/api/v1alpha1"

	capi "sigs.k8s.io/cluster-api/api/v1beta1"

	cpcapi "sigs.k8s.io/cluster-api/controlplane/kubeadm/api/v1beta1"
)

const CAPI_VERSION = "v1beta1"

var (
	appOwnerKey = ".metadata.clusterdef"
	apiGVStr    = gitopsv1.GroupVersion.String()
)

// ClusterDefReconciler reconciles a ClusterDef object
type ClusterDefReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
	Config *ClusterDefConfig
	State  map[string]*ClusterDefStateCache
}

// ClusterDefStateCache maintains the transient state of the reconciler.

type ClusterDefStateCache struct {
	// ClusterName is the name of target cluster-api definition
	ClusterName string
	// ClusterNamespace is the namespace of target cluster-api definition
	ClusterNamespace string
	// Kubeconfig is the config of target
	Kubeconfig *clientapi.Config
	// ServerURL is the cached URL of the workload cluster API endpoint
	ServerURL string
	// TargetClient is a kubernetes client for the target (workload) cluster
	TargetClient *clientgo.Clientset
	// isClusterPivoted
	IsClusterPivoted bool
	// isClusterReady
	IsClusterReady bool
	// PivotStatus
	PivotStatus gitopsv1.PivotState
}

// TransientState is the state of reconciler during the pivoting process
type TransientState int

const (
	// TSNotPivoted
	TSNotPivoted TransientState = iota
	// TSStoreTargetClusterInfo
	TSStoreTargetClusterInfo
	// TSUndeployLocalIronic
	TSUndeployLocalIronic
	// TSCheckIronicIsUndeployed
	TSCheckIronicIsUndeployed
	// TSInstallKanodOperator
	TSInstallKanodOperator
	// TSWaitForKanodOperator
	TSWaitForKanodOperator
	// TSMoveKanod
	TSMoveKanod
	// TSWaitForStackDeployed
	TSWaitForStackDeployed
	// TSDeleteArgoCdApp
	TSDeleteArgoCdApp
	// TSLabelResources
	TSLabelResources
	// TSPauseBaremetalpool
	TSPauseBaremetalpool
	// TSPauseNetwork
	TSPauseNetwork
	// TSPivotResources
	TSPivotResources
	// TSUnlabelResources
	TSUnlabelResources
	// TSPivotBmpResources
	TSPivotBmpResources
	// TSPivotNetworkResources
	TSPivotNetworkResources
	// TSActivateBaremetalpool
	TSActivateBaremetalpool
	// TSActivateNetwork
	TSActivateNetwork
	// TSRedeployLocalIronic
	TSRedeployLocalIronic
	// TSCheckIronicIsdeployed
	TSCheckIronicIsdeployed
	// TSConfigureArgoApp
	TSConfigureArgoApp
	// ClusterPivoted
	TSClusterPivoted
)

// ClusterDefConfig contains global parameters of the ClusterDef operator.
// Typical operators are the location and credentials of the snapshot repository,
// location of base cluster models and infrastructure parameters.
type ClusterDefConfig struct {
	ArgoCDNamespace   string
	ClusterFileName   string
	PluginName        string
	InfraNamespace    string
	KanodOperatorCrd  string
	MilliResyncPeriod int
}

type Env struct {
	Env    map[string]string
	Errors []string
}

func (e *Env) Getenv(key string) string {
	v, ok := e.Env[key]
	if !ok {
		e.Errors = append(e.Errors, key)
	}
	return v
}

func (e *Env) Setenv(key string, val string) {
	e.Env[key] = val
}

// MilliResyncPeriod is the duration between two synchronization attempt of a given resource
const MilliResyncPeriod = 10000

const PivotMilliResyncPeriod = 5000

// finalizerName is the name of the finalizer used for cascade resource deletion
const finalizerName = "resources-finalizer.gitops.kanod.io"

// bmpAnnotationPaused is the name of the annotation for pausing baremetalpool controller
const bmpAnnotationPaused = "baremetalpool.kanod.io/paused"

// networkAnnotationPaused is the name of the annotation for pausing network operator
const networkAnnotationPaused = "netpool.network.kanod.io/paused"

// baremetalpoolFinalizer is the name of the finalizer for baremetalpool resource
const baremetalpoolFinalizer = "baremetalpool.kanod.io/finalizer"

// networkFinalizer is the name of the finalizer for network resource
const networkFinalizer = "netpool.network.kanod.io/finalizer"

// LocalK8sApiServer is the address of the API server got the local cluster
const LocalK8sApiServer = "https://kubernetes.default.svc"

// clusterUrlAnnotation is the name of the annotation containing the pivoted cluster url
const clusterUrlAnnotation = "clusterdef.kanod.io/cluster-url"

// clusterPivotedAnnotation is the name of the annotation indicating if the cluster is pivoted
const clusterPivotedAnnotation = "clusterdef.kanod.io/cluster-pivoted"

// Name of the Ironic deployment
const ironicDeploymentName = "baremetal-operator-ironic"

// Namespace of the Ironic deployment
const ironicDeploymentNamespace = "baremetal-operator-system"

// Name of the Kanod operator deployment
const kanodDeploymentName = "kanod-operator-controller-manager"

// Namespace of the Kanod operator deployment
const kanodDeploymentNamespace = "kanod-operator-system"

// +kubebuilder:rbac:groups=gitops.kanod.io,resources=clusterdefs,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=gitops.kanod.io,resources=clusterdefs/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=argoproj.io,resources=applications,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="",resources=secrets,verbs=get;list;watch;create;update;delete;patch
// +kubebuilder:rbac:groups="",resources=configmaps,verbs=get;list;watch;create;update;delete
// +kubebuilder:rbac:groups="apiextensions.k8s.io",resources=customresourcedefinitions,verbs=get;list;watch;create;update;patch
// +kubebuilder:rbac:groups="cluster.x-k8s.io",resources="*",verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="infrastructure.cluster.x-k8s.io",resources="*",verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="addons.cluster.x-k8s.io",resources="*",verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="bootstrap.cluster.x-k8s.io",resources="*",verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="controlplane.cluster.x-k8s.io",resources="*",verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="metal3.io",resources="*",verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="ipam.metal3.io",resources="*",verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="clusterctl.cluster.x-k8s.io",resources="providers",verbs=get;list
// +kubebuilder:rbac:groups="bmp.kanod.io",resources="baremetalpools",verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="netpool.kanod.io",resources="networks",verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="config.kanod.io",resources="kanods",verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="apps",resources="deployment",verbs=get;list;update;patch
// +kubebuilder:rbac:groups="apps",resources="deployments",verbs=get;list;update;patch;watch

// Reconcile implements the core logic
func (r *ClusterDefReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := r.Log.WithValues("clusterdef", req.NamespacedName)
	var clusterDef gitopsv1.ClusterDef

	fullname := fmt.Sprintf("%s/%s", req.Namespace, req.Name)

	if err := r.Get(ctx, req.NamespacedName, &clusterDef); err != nil {
		delete(r.State, fullname)
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	if !clusterDef.ObjectMeta.DeletionTimestamp.IsZero() {
		return r.finalizeDeletion(req.Name, &clusterDef, log)
	}

	cache, presence := r.State[fullname]

	if !presence {
		cache = &ClusterDefStateCache{
			ClusterName:      "",
			ClusterNamespace: "",
			Kubeconfig:       nil,
			ServerURL:        LocalK8sApiServer,
			PivotStatus:      gitopsv1.NotPivoted,
			IsClusterPivoted: false,
			IsClusterReady:   false,
		}
		r.State[fullname] = cache
		log.Info("Initial registration in cache completed")
	}

	if !presence {
		if err := r.initializeState(ctx, &clusterDef, cache, log); err != nil {
			log.Error(err, "Problem while initializing state.")
			return ctrl.Result{}, err
		}
	}

	if err := r.makeCredentials(ctx, &clusterDef, log); err != nil {
		log.Error(err, "Problem with application credentials creation.")
		return ctrl.Result{}, err
	}

	if clusterDef.Spec.PivotInfo.Pivot && !cache.IsClusterPivoted {
		err, isReady := r.isClusterReadyForPivot(ctx, &clusterDef, cache, log)
		if err != nil {
			log.Error(err, "Problem while checking if cluster is ready for pivot.")
			return ctrl.Result{}, err
		}

		if isReady || cache.PivotStatus != gitopsv1.NotPivoted {
			return r.launchPivotProcess(ctx, log, cache, &clusterDef, req)
		}

	}

	serverUrl := cache.ServerURL
	if err := r.makeApplication(ctx, &clusterDef, serverUrl, log); err != nil {
		log.Error(err, "Problem with application creation.")
		return ctrl.Result{}, err
	}

	phase, cpVersion, versions := r.computeStatus(ctx, &req.NamespacedName, log, cache)

	if phase == gitopsv1.Ready {
		cache.IsClusterReady = true
	}

	if err := r.UpdateStatus(req.NamespacedName, phase, cpVersion, versions, cache.PivotStatus); err != nil {
		log.Error(err, "Problem while updating status.")
		return ctrl.Result{}, err
	}
	if needResync(&clusterDef, cpVersion, versions, log) {
		if err := r.syncApp(ctx, &req.NamespacedName, log); err != nil {
			log.Error(err, "Problem while forcing resync")
			return ctrl.Result{}, err
		}
	}

	return ctrl.Result{RequeueAfter: MilliResyncPeriod * time.Millisecond}, nil
}

// SetupWithManager registers the controller for ClusterDef
func (r *ClusterDefReconciler) SetupWithManager(mgr ctrl.Manager) error {

	if err := mgr.GetFieldIndexer().IndexField(context.Background(), &argocd.Application{}, appOwnerKey, indexFunc); err != nil {
		return err
	}

	return ctrl.NewControllerManagedBy(mgr).
		For(&gitopsv1.ClusterDef{}).
		Owns(&argocd.Application{}).
		Watches(
			&source.Kind{Type: &corev1.ConfigMap{}},
			handler.EnqueueRequestsFromMapFunc(r.MapObjectToClusterDef),
		).
		Watches(
			&source.Kind{Type: &capi.Machine{}},
			handler.EnqueueRequestsFromMapFunc(r.MapObjectToClusterDef),
		).
		Watches(
			&source.Kind{Type: &cpcapi.KubeadmControlPlane{}},
			handler.EnqueueRequestsFromMapFunc(r.MapObjectToClusterDef),
		).
		Complete(r)
}

// MakeConfig initializes the global configuration of the controller
func MakeConfig() *ClusterDefConfig {
	return &ClusterDefConfig{
		ArgoCDNamespace:  getEnvDefault("ARGOCD_NAMESPACE", "argocd"),
		PluginName:       getEnvDefault("ARGOCD_PLUGIN_NAME", "argocd-updater-plugin"),
		ClusterFileName:  getEnvDefault("CLUSTER_FILE_NAME", "config.yaml"),
		InfraNamespace:   getEnvDefault("INFRA_NAMESPACE", "cluster-def"),
		KanodOperatorCrd: getEnvDefault("KANOD_OPERATOR_CRD", ""),
	}
}

// indexFunc is an indexer for applications based on ownership by clusterdef
func indexFunc(rawObj client.Object) []string {
	app := rawObj.(*argocd.Application)
	owner := metav1.GetControllerOf(app)
	if owner == nil {
		return nil
	}
	if owner.APIVersion != apiGVStr || owner.Kind != "ClusterDef" {
		return nil
	}

	// ...and if so, return it
	return []string{owner.Name}
}

// Get cluster status after controller start
// and initialize cache
func (r *ClusterDefReconciler) initializeState(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	log logr.Logger,
) error {
	log.Info("Check cluster state after controller start")

	clusterUrl, present := clusterDef.Annotations[clusterUrlAnnotation]
	if !present {
		clusterUrl = LocalK8sApiServer
	}

	clusterPivoted, present := clusterDef.Annotations[clusterPivotedAnnotation]
	if !present {
		clusterPivoted = "false"
	}

	// check server address of the cluster
	if clusterPivoted == "true" {
		var cluster capi.Cluster
		argoSecretForTargetCluster := corev1.Secret{}
		secretKey := client.ObjectKey{
			Name:      fmt.Sprintf("%s-config", clusterDef.Name),
			Namespace: r.Config.ArgoCDNamespace,
		}
		if err := r.Get(ctx, secretKey, &argoSecretForTargetCluster); err != nil {
			if k8serrors.IsNotFound(err) {
				log.Error(err, "Argo secret for target cluster missing")
			}
			return err
		} else {
			config, err := createKubeconfigFromArgoSecret(clusterDef.Name, argoSecretForTargetCluster)
			if err != nil {
				log.Error(err, "Cannot create kubeconfig from argo secret")
				return err
			}

			restClient := getClientForCapi(config, &capi.GroupVersion)

			err = restClient.Get().Namespace(clusterDef.Namespace).Resource("clusters").Name(clusterDef.Name).Do(context.TODO()).Into(&cluster)
			if err != nil {
				log.Error(err, "Cannot get Cluster info")
				return err
			}
			cache.Kubeconfig = config
			cache.ServerURL = clusterUrl
			cache.IsClusterPivoted = true
			cache.PivotStatus = gitopsv1.ClusterPivoted
			cache.ClusterName = cluster.GetName()
			cache.ClusterNamespace = cluster.GetNamespace()

		}
	} else {
		cache.ServerURL = clusterUrl
		cache.IsClusterPivoted = false
		cache.PivotStatus = gitopsv1.NotPivoted
		cache.ClusterName = clusterDef.Name
		cache.ClusterNamespace = clusterDef.Namespace
	}

	log.Info("clusterdef controller initialized", "clusterUrl :", clusterUrl)
	log.Info("clusterdef controller initialized", "clusterPivoted :", clusterPivoted)

	return nil
}

func (r *ClusterDefReconciler) launchPivotProcess(
	ctx context.Context,
	log logr.Logger,
	cache *ClusterDefStateCache,
	clusterDef *gitopsv1.ClusterDef,
	req ctrl.Request,
) (ctrl.Result, error) {

	step, recDate, err := r.getProgressionWithClient(ctx, req.NamespacedName)
	if err != nil {
		r.setProgression(ctx, clusterDef, 0)
		return ctrl.Result{}, err
	}

	log.Info("Progression : ", "step", step, "recDate", recDate)
	switch step {
	case TSNotPivoted:
		log.Info("Pause clusterapi")
		err = r.pauseCapi(ctx, clusterDef, log)
		if err != nil {
			log.Error(err, "Problem while pausing clusterapi.")
			return ctrl.Result{}, err
		}
		if err := r.setProgression(ctx, clusterDef, TSStoreTargetClusterInfo); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSStoreTargetClusterInfo")
			return ctrl.Result{}, err
		}

	case TSStoreTargetClusterInfo:
		log.Info("StoreTargetClusterInfo...")
		if err := r.StoreTargetClusterInfo(ctx, cache, &req.NamespacedName, log); err != nil {
			log.Error(err, "Problem while get target cluster info")
			return ctrl.Result{}, err
		}

		statusValue := gitopsv1.TargetClusterInfoStored
		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}
		cache.PivotStatus = statusValue

		if err := r.setProgression(ctx, clusterDef, TSUndeployLocalIronic); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSUndeployLocalIronic")
			return ctrl.Result{}, err
		}

	case TSUndeployLocalIronic:
		log.Info("scale ironic deployment to 0 on local cluster")
		err := r.scaleLocalIronicDeployment(ctx, 0, log)
		if err != nil {
			log.Error(err, "Problem while scaling to 0 ironic deployment cluster")
			return ctrl.Result{}, err
		}

		statusValue := gitopsv1.IronicOnLocalClusterScaledToZero

		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}

		cache.PivotStatus = statusValue
		if err := r.setProgression(ctx, clusterDef, TSCheckIronicIsUndeployed); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSCheckIronicIsUndeployed")
			return ctrl.Result{}, err
		}

	case TSCheckIronicIsUndeployed:
		log.Info("Wait for ironic deployment being deployed")
		err, isIronicUndeployed := r.isIronicDeploymentReady(ctx, log)
		if err != nil {
			log.Error(err, "Problem while checking if ironic is undeployed")
			return ctrl.Result{}, err
		}

		if !isIronicUndeployed {
			return ctrl.Result{RequeueAfter: PivotMilliResyncPeriod * time.Millisecond}, nil
		}

		statusValue := gitopsv1.IronicOnLocalClusterUndeployed

		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}

		cache.PivotStatus = statusValue
		if err := r.setProgression(ctx, clusterDef, TSInstallKanodOperator); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSInstallKanodOperator")
			return ctrl.Result{}, err
		}

	case TSInstallKanodOperator:
		log.Info("Installing kanod operator...")
		if err := r.installKanodOperator(ctx, clusterDef, cache, log); err != nil {
			log.Error(err, "Problem while moving kanod custom resource on target cluster")
			return ctrl.Result{}, err
		}

		statusValue := gitopsv1.KanodOperatorInstalled
		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}
		cache.PivotStatus = statusValue

		if err := r.setProgression(ctx, clusterDef, TSWaitForKanodOperator); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSWaitForKanodOperator")
			return ctrl.Result{}, err
		}

	case TSWaitForKanodOperator:
		log.Info("waiting for kanod operator...")
		err, isDeployed := r.isKanodDeployed(ctx, clusterDef, cache, log)
		if err != nil {
			log.Error(err, "Problem while checking if kanod operator is deployed on target cluster")
			return ctrl.Result{}, err
		}

		if !isDeployed {
			return ctrl.Result{RequeueAfter: PivotMilliResyncPeriod * time.Millisecond}, nil
		}

		statusValue := gitopsv1.KanodOperatorReady
		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}
		cache.PivotStatus = statusValue

		if err := r.setProgression(ctx, clusterDef, TSMoveKanod); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSMoveKanod")
			return ctrl.Result{}, err
		}

	case TSMoveKanod:
		log.Info("Moving kanod resource...")
		if err := r.createNamespace(ctx, clusterDef, cache, clusterDef.Spec.PivotInfo.KanodNamespace, log); err != nil {
			log.Error(err, "Error while creating namespace for kanod on target cluster")
			return ctrl.Result{}, err
		}

		if err := r.moveKanodResources(ctx, clusterDef, cache, log); err != nil {
			log.Error(err, "Problem while moving kanod custom resource on target cluster")
			return ctrl.Result{}, err
		}

		statusValue := gitopsv1.KanodResourcesMoved
		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}
		cache.PivotStatus = statusValue

		if err := r.setProgression(ctx, clusterDef, TSWaitForStackDeployed); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSWaitForStackDeployed")
			return ctrl.Result{}, err
		}

	case TSWaitForStackDeployed:
		log.Info("Wait for stack deployment...")
		err, isStackDeployed := r.isStackDeployed(ctx, clusterDef, cache, log)
		if err != nil {
			log.Error(err, "Problem while checking if stack is deployed on target cluster")
			return ctrl.Result{}, err
		}

		if !isStackDeployed {
			return ctrl.Result{RequeueAfter: PivotMilliResyncPeriod * time.Millisecond}, nil
		}

		statusValue := gitopsv1.StackDeployed
		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}
		cache.PivotStatus = statusValue

		if err := r.setProgression(ctx, clusterDef, TSDeleteArgoCdApp); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSDeleteArgoCdApp")
			return ctrl.Result{}, err
		}

	case TSDeleteArgoCdApp:
		log.Info("Delete ArgoCD application")
		appName := fmt.Sprintf("cdef-%s", clusterDef.Name)
		r.deleteArgocdApp(ctx, appName, log)

		statusValue := gitopsv1.ArgoAppDeleted
		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}
		cache.PivotStatus = statusValue
		if err := r.setProgression(ctx, clusterDef, TSLabelResources); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSLabelResources")
			return ctrl.Result{}, err
		}

	case TSLabelResources:
		log.Info("Add labels on resources for clusterctl move")
		if err := r.labelResources(ctx, log, true); err != nil {
			log.Error(err, "Problem while labeling resources.")
			return ctrl.Result{}, err
		}

		statusValue := gitopsv1.ResourcesLabeled

		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}
		cache.PivotStatus = statusValue
		if err := r.setProgression(ctx, clusterDef, TSPauseBaremetalpool); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSPauseBaremetalpool")
			return ctrl.Result{}, err
		}

	case TSPauseBaremetalpool:
		log.Info("Pause baremetalpool on local cluster")
		if err := r.pauseBaremetalpool(ctx, clusterDef, cache, log); err != nil {
			log.Error(err, "Problem while pausing baremetalpool")
			return ctrl.Result{}, err
		}

		statusValue := gitopsv1.BaremetalpoolPaused

		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}
		cache.PivotStatus = statusValue
		if err := r.setProgression(ctx, clusterDef, TSPauseNetwork); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSPauseNetwork")
			return ctrl.Result{}, err
		}

	case TSPauseNetwork:
		log.Info("Pause network operator on local cluster")
		if err := r.pauseNetwork(ctx, clusterDef, cache, log); err != nil {
			log.Error(err, "Problem while pausing network operator")
			return ctrl.Result{}, err
		}

		statusValue := gitopsv1.NetworkPaused

		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}
		cache.PivotStatus = statusValue
		if err := r.setProgression(ctx, clusterDef, TSPivotResources); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSPivotResources")
			return ctrl.Result{}, err
		}

	case TSPivotResources:
		log.Info("Pivot resources on target cluster")
		if err := r.moveClusterResources(ctx, cache, &req.NamespacedName, log); err != nil {
			log.Error(err, "Problem while moving resources on target cluster")
			return ctrl.Result{}, err
		}

		statusValue := gitopsv1.ResourcesPivoted

		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}

		cache.PivotStatus = statusValue
		if err := r.setProgression(ctx, clusterDef, TSUnlabelResources); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSUnlabelResources")
			return ctrl.Result{}, err
		}

	case TSUnlabelResources:
		log.Info("Remove labels on resources")
		if err := r.labelResources(ctx, log, false); err != nil {
			log.Error(err, "Problem while labeling resources.")
			return ctrl.Result{}, err
		}

		statusValue := gitopsv1.ResourcesUnlabeled

		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}

		cache.PivotStatus = statusValue
		if err := r.setProgression(ctx, clusterDef, TSPivotBmpResources); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSPivotBmpResources")
			return ctrl.Result{}, err
		}

	case TSPivotBmpResources:
		log.Info("Pivot baremetalpool resources on target cluster")
		if err := r.moveBaremetalpoolResources(ctx, clusterDef, cache, log); err != nil {
			log.Error(err, "Problem while moving baremetalpool on target cluster")
			return ctrl.Result{}, err
		}

		statusValue := gitopsv1.BmpResourcesPivoted

		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}

		cache.PivotStatus = statusValue
		if err := r.setProgression(ctx, clusterDef, TSPivotNetworkResources); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSPivotNetworkResources")
			return ctrl.Result{}, err
		}

	case TSPivotNetworkResources:
		log.Info("Pivot network resources on target cluster")
		if err := r.moveNetworkResources(ctx, clusterDef, cache, log); err != nil {
			log.Error(err, "Problem while moving network on target cluster")
			return ctrl.Result{}, err
		}

		statusValue := gitopsv1.NetworkResourcesPivoted

		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}

		cache.PivotStatus = statusValue
		if err := r.setProgression(ctx, clusterDef, TSActivateBaremetalpool); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSActivateBaremetalpool")
			return ctrl.Result{}, err
		}

	case TSActivateBaremetalpool:
		log.Info("unpause baremetalpool resources on target cluster")
		err, ok := r.unpauseBaremetalpoolResources(ctx, clusterDef, cache, log)
		if err != nil {
			log.Error(err, "Problem while unpausing baremetalpool on target cluster")
			return ctrl.Result{}, err
		}

		if !ok {
			log.Info("waiting for deletion of baremetalpool on local cluster")
			return ctrl.Result{RequeueAfter: PivotMilliResyncPeriod * time.Millisecond}, err
		}
		statusValue := gitopsv1.BaremetalpoolUnPaused

		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}

		cache.PivotStatus = statusValue
		if err := r.setProgression(ctx, clusterDef, TSActivateNetwork); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSActivateNetwork")
			return ctrl.Result{}, err
		}

	case TSActivateNetwork:
		log.Info("unpause network operator resources on target cluster")
		err, ok := r.unpauseNetworkResources(ctx, clusterDef, cache, log)
		if err != nil {
			log.Error(err, "Problem while unpausing network operator on target cluster")
			return ctrl.Result{}, err
		}

		if !ok {
			log.Info("waiting for deletion of network operator on local cluster")
			return ctrl.Result{RequeueAfter: PivotMilliResyncPeriod * time.Millisecond}, err
		}
		statusValue := gitopsv1.NetworkUnPaused

		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}

		cache.PivotStatus = statusValue
		if err := r.setProgression(ctx, clusterDef, TSRedeployLocalIronic); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSRedeployLocalIronic")
			return ctrl.Result{}, err
		}

	case TSRedeployLocalIronic:
		log.Info("scale ironic deployment to 1 on local cluster")
		err := r.scaleLocalIronicDeployment(ctx, 1, log)
		if err != nil {
			log.Error(err, "Problem while scaling to 1 ironic deployment cluster")
			return ctrl.Result{}, err
		}

		statusValue := gitopsv1.IronicOnLocalClusterScaledToOne

		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}

		cache.PivotStatus = statusValue
		if err := r.setProgression(ctx, clusterDef, TSCheckIronicIsdeployed); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSCheckIronicIsdeployed")
			return ctrl.Result{}, err
		}

	case TSCheckIronicIsdeployed:
		log.Info("Wait for ironic deployment being deployed")
		err, isIronicRedeployed := r.isIronicDeploymentReady(ctx, log)
		if err != nil {
			log.Error(err, "Problem while checking if ironic is redeployed")
			return ctrl.Result{}, err
		}

		if !isIronicRedeployed {
			return ctrl.Result{RequeueAfter: PivotMilliResyncPeriod * time.Millisecond}, nil
		}

		statusValue := gitopsv1.IronicOnLocalClusterRedeployed

		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}

		cache.PivotStatus = statusValue
		if err := r.setProgression(ctx, clusterDef, TSConfigureArgoApp); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSConfigureArgoApp")
			return ctrl.Result{}, err
		}

	case TSConfigureArgoApp:
		log.Info("Configure ArgoCD application")
		if err := r.UpdateArgoCDConfig(ctx, cache, log); err != nil {
			log.Error(err, "Problem while changing destination cluster for argocd application")
			return ctrl.Result{}, err
		}

		statusValue := gitopsv1.ClusterPivoted

		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}

		cache.IsClusterPivoted = true
		cache.PivotStatus = statusValue
		if err := r.setProgression(ctx, clusterDef, TSClusterPivoted); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSClusterPivoted")
			return ctrl.Result{}, err
		}
		if err := r.setAnnotation(ctx, clusterDef, clusterUrlAnnotation, cache.ServerURL); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for cluster-url")
			return ctrl.Result{}, err
		}
		if err := r.setAnnotation(ctx, clusterDef, clusterPivotedAnnotation, "true"); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for cluster-pivoted")
			return ctrl.Result{}, err
		}

	}
	return ctrl.Result{RequeueAfter: PivotMilliResyncPeriod * time.Millisecond}, nil
}

func (r *ClusterDefReconciler) isClusterReadyForPivot(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	log logr.Logger,
) (error, bool) {

	var machines capi.MachineList

	isReady := true

	err := r.Client.List(ctx, &machines, client.MatchingLabels{"cluster.x-k8s.io/cluster-name": clusterDef.Name})

	if err != nil {
		if !k8serrors.IsNotFound(err) {
			log.Error(err, "Cannot list machines in cluster.")
			return err, false
		}
	}

	for _, machine := range machines.Items {
		if machine.Status.Phase != "Running" {
			log.Info("machine not in running phase", "machine", machine.Name)
			isReady = false
			return nil, isReady
		}
		for _, condition := range machine.Status.Conditions {
			if condition.Type == capi.ReadyCondition && condition.Status != corev1.ConditionTrue {
				log.Info("machine not in ready state", "machine", machine.Name)
				isReady = false
				return nil, isReady
			}
		}
	}
	return nil, isReady
}
