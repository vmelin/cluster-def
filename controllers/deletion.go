/*
Copyright 2022 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"

	"github.com/go-logr/logr"
	gitopsv1 "gitlab.com/Orange-OpenSource/kanod/cluster-def/api/v1alpha1"
	argocd "gitlab.com/Orange-OpenSource/kanod/cluster-def/thirdparty/argocd/apis/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
)

// finalizeDeletion finishes deletion by handling annotations for cascaded
// deletion in the application
func (r *ClusterDefReconciler) finalizeDeletion(
	name string,
	clusterDef *gitopsv1.ClusterDef,
	log logr.Logger,
) (ctrl.Result, error) {
	log.V(0).Info(
		"Finalize deletion of clusterdef",
		"ClusterDef", name,
	)
	appName := fmt.Sprintf("cdef-%s", clusterDef.Name)
	ctx := context.Background()

	if containsString(clusterDef.ObjectMeta.Finalizers, finalizerName) {
		// add the finalizer to the Applications if they exist to enable cascade deletion.
		log.V(0).Info(
			"Cascaded deletion requested",
			"ClusterDef", name,
		)
		r.addApplicationFinalizer(ctx, appName, log)

		// remove the finalizer from the list and update it.
		clusterDef.ObjectMeta.Finalizers = removeString(clusterDef.ObjectMeta.Finalizers, finalizerName)
		if err := r.Update(ctx, clusterDef); err != nil {
			return ctrl.Result{}, err
		}

	} else {
		log.V(0).Info(
			"Simple deletion requested",
			"ClusterDef", name,
		)
	}
	// delete argocd application.
	r.deleteArgocdApp(ctx, appName, log)
	r.deleteCredentials(ctx, appName, log)

	return ctrl.Result{}, nil
}

/* addApplicationFinalizer adds a finalizer to the application to force a
   cascaded deletion. Otherwise only the application is destroyed, not the
   it defines */
func (r *ClusterDefReconciler) addApplicationFinalizer(
	ctx context.Context,
	appName string,
	log logr.Logger,
) error {
	application := &argocd.Application{}
	appMeta := client.ObjectKey{Name: appName, Namespace: "argocd"}
	log.V(0).Info(
		"Trying to find application for deletion",
		"Application", appName,
	)
	if err := r.Client.Get(ctx, appMeta, application); err == nil {
		log.V(0).Info(
			"Add finalizer to ArgoCD application",
			"Application", appName,
		)
		controllerutil.AddFinalizer(application, argocd.BackgroundPropagationPolicyFinalizer)
		if err := r.Client.Update(ctx, application); err != nil {
			log.Error(err, "Cannot update finalizer", "Application", appName)
			return err
		}
	} else if !k8serrors.IsNotFound(err) {
		log.Error(err, "Cannot access application", "Application", appName)
		return err
	}
	return nil
}

func (r *ClusterDefReconciler) deleteArgocdApp(
	ctx context.Context,
	appName string,
	log logr.Logger,
) error {
	application := &argocd.Application{}
	appMeta := client.ObjectKey{Name: appName, Namespace: "argocd"}
	log.V(0).Info(
		"Trying to delete argocd application",
		"Application", appName,
	)
	if err := r.Client.Get(ctx, appMeta, application); err == nil {
		/* Delete argocd application created by clusterdef */
		if err := r.Client.Delete(ctx, application); err != nil {
			log.Error(err, "Cannot delete application", "Application", appName)
			return err
		}
	} else if !k8serrors.IsNotFound(err) {
		log.Error(err, "Cannot access application", "Application", appName)
		return err
	}
	return nil
}

func (r *ClusterDefReconciler) deleteCredentials(
	ctx context.Context,
	secName string,
	log logr.Logger,
) error {
	secret := &corev1.Secret{}
	secMeta := client.ObjectKey{Name: secName, Namespace: "argocd"}
	log.V(0).Info(
		"Trying to delete argocd secret",
		"secret", secName,
	)
	if err := r.Client.Get(ctx, secMeta, secret); err == nil {
		/* Delete argocd secret created by clusterdef */
		if err := r.Client.Delete(ctx, secret); err != nil {
			log.Error(err, "Cannot delete secret", "secret", secName)
			return err
		}
	} else if !k8serrors.IsNotFound(err) {
		log.Error(err, "Cannot access secret", "secret", secName)
		return err
	}
	return nil
}
