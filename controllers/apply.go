package controllers

import (
	"context"
	"crypto/tls"
	"fmt"
	"io"
	"net/http"
	"strings"

	"github.com/drone/envsubst"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/util/yaml"
	"k8s.io/client-go/discovery/cached/memory"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/restmapper"
	"k8s.io/client-go/tools/clientcmd"
)

func (r *ClusterDefReconciler) ApplyFile(ctx context.Context, env *Env, url string, cache *ClusterDefStateCache) error {
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	client := &http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		r.Log.Error(err, "cannot create resource request", "url", url)
		return err
	}

	resp, err := client.Do(req)
	if err != nil {
		r.Log.Error(err, "cannot read resource", "url", url)
		return err
	}
	if resp.StatusCode != 200 {
		err = fmt.Errorf("bad status code: %d", resp.StatusCode)
		r.Log.Error(err, "read resource failed")
		return err
	}
	defer resp.Body.Close()

	data, err := io.ReadAll(resp.Body)
	if err != nil {
		r.Log.Error(err, "cannot extract resource body", "url", url)
		return err
	}
	return r.ApplyData(ctx, env, url, data, cache)
}

func (r *ClusterDefReconciler) ApplyData(ctx context.Context, env *Env, resource string, data []byte, cache *ClusterDefStateCache) error {
	buf, err := envsubst.Eval(string(data), env.Getenv)
	if len(env.Errors) > 0 {
		r.Log.Info("Missing substitution for variables", "variables", strings.Join(env.Errors, ", "))
		env.Errors = nil
	}
	if err != nil {
		r.Log.Error(err, "envsubst failed", "origin", resource)
		return err
	}

	clientConfig := clientcmd.NewDefaultClientConfig(
		*cache.Kubeconfig,
		&clientcmd.ConfigOverrides{},
	)
	restConfigTarget, _ := clientConfig.ClientConfig()

	dynclient, err := dynamic.NewForConfig(restConfigTarget)
	if err != nil {
		r.Log.Error(err, "cannot get dynclient")
		return err
	}
	clientset, err := kubernetes.NewForConfig(restConfigTarget)
	if err != nil {
		r.Log.Error(err, "cannot get clientset")
		return err
	}
	discoveryClient := clientset.DiscoveryClient
	cachedDiscoveryClient := memory.NewMemCacheClient(discoveryClient)
	if err != nil {
		r.Log.Error(err, "cannot get dynamic client")
		return err
	}

	for pos, resource := range strings.Split(buf, "\n---\n") {
		u := &unstructured.Unstructured{Object: map[string]interface{}{}}
		if err := yaml.Unmarshal([]byte(resource), &u); err != nil {
			r.Log.Error(err, "cannot unmarshal resource", "origin", resource, "pos", pos)
			return err
		}
		if u == nil || u.Object == nil {
			r.Log.Info("Found an empty object. Skipping.")
			continue
		}
		gvk := u.GroupVersionKind()

		mapper := restmapper.NewDeferredDiscoveryRESTMapper(cachedDiscoveryClient)
		mapping, err := mapper.RESTMapping(gvk.GroupKind(), gvk.Version)
		if err != nil {
			r.Log.Error(err, "cannot get the gvr")
			return err
		}
		name := u.GetName()
		namespace := u.GetNamespace()
		if strings.HasPrefix(gvk.Kind, "ClusterRole") {
			namespace = ""
		}
		dync := dynclient.Resource(mapping.Resource).Namespace(namespace)
		if _, err := dync.Get(ctx, name, metav1.GetOptions{}); k8serrors.IsNotFound(err) {
			r.Log.Info(
				"Creating resource",
				"name", name, "namespace", namespace, "group",
				gvk.Group, "version", gvk.Version, "kind", gvk.Kind)
			_, err = dync.Create(ctx, u, metav1.CreateOptions{})
			if err != nil {
				r.Log.Error(err, "cannot create the object dynamically", "content", resource)
				return err
			}
		} else if err != nil {
			r.Log.Error(err, "failure during dynamic get", "name", name, "namespace", namespace, "kind", gvk.Kind)
		}

	}
	return nil
}
