/*
Copyright 2022 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"encoding/base64"
	"fmt"
	"os"

	appsv1 "k8s.io/api/apps/v1"

	"github.com/go-logr/logr"
	"github.com/pkg/errors"
	gitopsv1 "gitlab.com/Orange-OpenSource/kanod/cluster-def/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	ctrl "sigs.k8s.io/controller-runtime"

	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/rest"
	clctl "sigs.k8s.io/cluster-api/cmd/clusterctl/client"
	logf "sigs.k8s.io/cluster-api/cmd/clusterctl/log"
	capiversion "sigs.k8s.io/cluster-api/version"

	//	clientgo "k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
	clientapi "k8s.io/client-go/tools/clientcmd/api"
	capi "sigs.k8s.io/cluster-api/api/v1beta1"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	// kanodv1 "gitlab.com/Orange-OpenSource/kanod/kanod-operator/api/v1"
)

func (r *ClusterDefReconciler) StoreTargetClusterInfo(
	ctx context.Context,
	cache *ClusterDefStateCache,
	cdefName *types.NamespacedName,
	log logr.Logger,
) error {
	var cluster capi.Cluster

	key := types.NamespacedName{
		Name:      cdefName.Name,
		Namespace: cdefName.Namespace,
	}
	if err := r.Client.Get(ctx, key, &cluster); err != nil {
		if k8serrors.IsNotFound(err) {
			log.Info("Cannot store target cluster info, cannot get cluster yet")
		} else {
			log.Error(err, "Cannot get Cluster.")
		}
		return err
	}
	cache.ClusterName = cluster.GetName()
	cache.ClusterNamespace = cluster.GetNamespace()

	secret := &corev1.Secret{}
	key = client.ObjectKey{
		Namespace: cache.ClusterNamespace,
		Name:      fmt.Sprintf("%s-kubeconfig", cache.ClusterName),
	}
	if err := r.Get(ctx, key, secret); err != nil {
		log.V(0).Info("unable to get Kubeconfig secret yet")
		return err
	} else {
		kcData, presence := secret.Data["value"]
		if !presence {
			log.Error(err, "unable to find kubeconfig info")
			return err
		}
		log.V(0).Info("Found kubeconfig")
		if config, err := clientcmd.Load(kcData); err != nil {
			log.Error(err, "unable to internalize kubeconfig info")
		} else {
			cache.Kubeconfig = config
		}
	}
	kubeConfig := cache.Kubeconfig
	currentContext, presence := kubeConfig.Contexts[kubeConfig.CurrentContext]
	if !presence {
		return errors.New("cannot find current context")
	}
	server, presence := kubeConfig.Clusters[currentContext.Cluster]
	if !presence {
		return errors.New("cannot find current server")
	}
	cache.ServerURL = server.Server

	return nil
}

// Create in the cluster the secret for ArgoCD
func (r *ClusterDefReconciler) UpdateArgoCDConfig(
	ctx context.Context,
	cache *ClusterDefStateCache,
	log logr.Logger,
) error {

	kubeConfig := cache.Kubeconfig
	clusterName := cache.ClusterName

	// Create the secret containing target cluster configuration.
	secret, _, err := r.createArgoTargetConfig(clusterName, kubeConfig)
	if err != nil {
		return fmt.Errorf("cannot generate argocd secret for target cluster: %w", err)
	}

	log.V(0).Info("Creating argocd secret for target cluster")
	if _, err := controllerutil.CreateOrUpdate(ctx, r.Client, secret, func() error { return nil }); err != nil {
		return fmt.Errorf("cannot create argocd secret on target cluster: %w", err)
	}

	return nil
}

// Create the secret containing the KubeConfig of target cluster for ArgoCD
func (r *ClusterDefReconciler) createArgoTargetConfig(
	clusterName string, // name of the target cluster
	kubeconfig *clientapi.Config, // kubeconfig of the client
) (*corev1.Secret, string, error) {
	currentContext, presence := kubeconfig.Contexts[kubeconfig.CurrentContext]
	if !presence {
		return nil, "", errors.New("cannot find current context")
	}
	user, presence := kubeconfig.AuthInfos[currentContext.AuthInfo]
	if !presence {
		return nil, "", errors.New("cannot find current user infos")
	}
	server, presence := kubeconfig.Clusters[currentContext.Cluster]
	if !presence {
		return nil, "", errors.New("cannot find current server")
	}
	if server.CertificateAuthorityData == nil {
		return nil, "", errors.New("cannot find CA")
	}
	if user.ClientKeyData == nil {
		return nil, "", errors.New("cannot find user key")
	}
	if user.ClientCertificateData == nil {
		return nil, "", errors.New("cannot find user certificate")
	}
	caData := base64.StdEncoding.EncodeToString(server.CertificateAuthorityData)
	keyData := base64.StdEncoding.EncodeToString(user.ClientKeyData)
	certData := base64.StdEncoding.EncodeToString(user.ClientCertificateData)
	config := fmt.Sprintf(
		"{\"tlsClientConfig\":{\"insecure\":false,\"certData\":\"%s\",\"keyData\":\"%s\",\"caData\":\"%s\"}}",
		certData, keyData, caData)
	data := make(map[string][]byte)
	data["config"] = []byte(config)
	data["server"] = []byte(server.Server)
	data["name"] = []byte(clusterName)

	secret := &corev1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      fmt.Sprintf("%s-config", clusterName),
			Namespace: r.Config.ArgoCDNamespace,
			Labels: map[string]string{
				"argocd.argoproj.io/secret-type": "cluster",
			},
		},
		Data: data,
	}
	return secret, server.Server, nil
}

// Label/unlabel the resources for the clusterctl move process
func (r *ClusterDefReconciler) labelResources(
	ctx context.Context,
	log logr.Logger,
	addLabelFlag bool,
) error {
	restConfig := ctrl.GetConfigOrDie()
	dynClient, err := dynamic.NewForConfig(restConfig)
	if err != nil {
		log.Error(err, "Problem while creating cluster config.")
		return err
	}

	gvr := schema.GroupVersionResource{
		Group:    "apiextensions.k8s.io",
		Version:  "v1",
		Resource: "customresourcedefinitions",
	}

	options := metav1.ListOptions{
		LabelSelector: "cluster.x-k8s.io/provider",
		Limit:         50,
	}

	rsc, err := dynClient.Resource(gvr).List(ctx, options)
	if err != nil {
		log.Error(err, "Problem while listing resources.")
		return err
	}

	if err = labelRsrc(dynClient, ctx, log, gvr, "baremetalhosts.metal3.io", "clusterctl.cluster.x-k8s.io", addLabelFlag); err != nil {
		log.Error(err, "Problem while labeling baremetalhosts.metal3.io CRD resource.")
		return err
	}

	// add label on unused baremetalhosts in order to move them
	if err = labelRsrc(dynClient, ctx, log, gvr, "baremetalhosts.metal3.io", "clusterctl.cluster.x-k8s.io/move-hierarchy", addLabelFlag); err != nil {
		log.Error(err, "Problem while labeling baremetalhosts.metal3.io CRD resource.")
		return err
	}

	if err = labelRsrc(dynClient, ctx, log, gvr, "hardwaredata.metal3.io", "clusterctl.cluster.x-k8s.io", addLabelFlag); err != nil {
		log.Error(err, "Problem while labeling baremetalhosts.metal3.io CRD resource.")
		return err
	}

	if err = labelRsrc(dynClient, ctx, log, gvr, "hardwaredata.metal3.io", "clusterctl.cluster.x-k8s.io/move", addLabelFlag); err != nil {
		log.Error(err, "Problem while labeling hardwaredata.metal3.io CRD resource.")
		return err
	}

	for _, crd := range rsc.Items {
		metadata := crd.Object["metadata"].(map[string]interface{})
		crdName := fmt.Sprintf("%v", metadata["name"])

		if err = labelRsrc(dynClient, ctx, log, gvr, crdName, "clusterctl.cluster.x-k8s.io", addLabelFlag); err != nil {
			log.Error(err, "Problem while labeling resource resource.")
			return err

		}
	}
	return nil
}

// Label/unlabel a specific resource
func labelRsrc(dynclient dynamic.Interface,
	ctx context.Context,
	log logr.Logger,
	gvr schema.GroupVersionResource,
	name string,
	label string,
	addLabelFlag bool,
) error {
	result, err := dynclient.Resource(gvr).Get(ctx, name, metav1.GetOptions{})
	if err != nil {
		log.Error(err, "Problem while getting resource.")
		return err
	}

	var tmp map[string]string
	tmp = result.GetLabels()
	if addLabelFlag {
		log.Info(fmt.Sprintf("Adding label %s on resource %s", label, name))
		if tmp == nil {
			tmp = map[string]string{label: ""}
		} else {
			tmp[label] = ""
		}
	} else {
		log.Info(fmt.Sprintf("Deleting label %s on resource %s", label, name))
		delete(tmp, label)
	}

	result.SetLabels(tmp)
	_, err = dynclient.Resource(gvr).Update(ctx, result, metav1.UpdateOptions{})
	if err != nil {
		log.Error(err, "Problem while updating resource.")
		return err
	}
	return nil
}

// move labelled resources to the target cluster with clusterctl api
func (r *ClusterDefReconciler) moveClusterResources(
	ctx context.Context,
	cache *ClusterDefStateCache,
	cdefName *types.NamespacedName,
	log logr.Logger) error {

	restConfig, _ := rest.InClusterConfig()
	config := generateKubeconfigFromRestconfig(restConfig)

	fKcLocal, err := os.CreateTemp("", "kc-local")
	if err != nil {
		log.Error(err, "Problem while creating temp file.")
		return err
	}

	defer os.Remove(fKcLocal.Name())

	fKcTarget, err := os.CreateTemp("", "kc-target")
	if err != nil {
		log.Error(err, "Problem while creating temp file.")
		return err
	}

	defer os.Remove(fKcTarget.Name())

	err = clientcmd.WriteToFile(*config, fKcLocal.Name())
	if err != nil {
		log.Error(err, "Problem while writting local kubeconfig file.")
		return err
	}

	configTarget := cache.Kubeconfig

	err = clientcmd.WriteToFile(*configTarget, fKcTarget.Name())
	if err != nil {
		log.Error(err, "Problem while writting target kubeconfig file.")
		return err
	}

	c, err := clctl.New("")
	if err != nil {
		log.Error(err, "Problem while creating clusterctl client.")
		return err
	}
	verbosity := 5
	logf.SetLogger(logf.NewLogger(logf.WithThreshold(&verbosity)))
	namespace := cdefName.Namespace

	log.Info(fmt.Sprintf("clusterapi version : %s", capiversion.Get().GitVersion))
	err = c.Move(clctl.MoveOptions{
		FromKubeconfig: clctl.Kubeconfig{Path: fKcLocal.Name(), Context: ""},
		ToKubeconfig:   clctl.Kubeconfig{Path: fKcTarget.Name(), Context: ""},
		Namespace:      namespace,
		DryRun:         false,
	})

	if err != nil {
		log.Error(err, "Problem while moving resources.")
		return err
	}

	return nil
}

// Move baremetalpool resources listed in the clusterdef spec to the target cluster
func (r *ClusterDefReconciler) moveBaremetalpoolResources(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	log logr.Logger) error {

	restConfigLocal, _ := rest.InClusterConfig()
	dynClientLocal, err := dynamic.NewForConfig(restConfigLocal)
	if err != nil {
		log.Error(err, "Problem while creating client for local cluster.")
		return err
	}

	clientConfig := clientcmd.NewDefaultClientConfig(
		*cache.Kubeconfig,
		&clientcmd.ConfigOverrides{},
	)

	restConfigTarget, _ := clientConfig.ClientConfig()

	dynClientTarget, err := dynamic.NewForConfig(restConfigTarget)
	if err != nil {
		log.Error(err, "Problem while creating client for target cluster.")
		return err
	}

	gvrbmp := schema.GroupVersionResource{
		Group:    "bmp.kanod.io",
		Version:  "v1",
		Resource: "baremetalpools",
	}

	gvrsecret := schema.GroupVersionResource{
		Group:    "",
		Version:  "v1",
		Resource: "secrets",
	}

	bmpNamespace := cache.ClusterNamespace

	for _, bmpName := range clusterDef.Spec.PivotInfo.BaremetalpoolList {
		bmp, err := dynClientLocal.Resource(gvrbmp).Namespace(bmpNamespace).Get(ctx, bmpName, metav1.GetOptions{})
		if err == nil {
			spec := bmp.Object["spec"].(map[string]interface{})
			credentialName := fmt.Sprintf("%v", spec["credentialName"])

			log.Info(fmt.Sprintf("Prepare moving baremetalpool resource %s with secret %s", bmpName, credentialName))

			// get secret associated with the baremetalpool on local cluster
			secret, err := dynClientLocal.Resource(gvrsecret).Namespace(bmpNamespace).Get(ctx, credentialName, metav1.GetOptions{})
			if err != nil {
				log.Error(err, "error while getting baremetalpool secret on local cluster")
				return err
			}

			// create secret associated with the baremetalpool on target cluster
			log.Info(fmt.Sprintf("creating secret %s on target cluster", credentialName))
			secret.SetResourceVersion("")
			_, err = dynClientTarget.Resource(gvrsecret).Namespace(bmpNamespace).Create(ctx, secret, metav1.CreateOptions{})
			if err != nil {
				log.Error(err, "error while creating baremetalpool secret on target cluster")
				return err
			}

			// remove baremetalpool on local cluster and create baremetalpool on target cluster
			// remove pause annotation on baremetalpool
			annotations := bmp.GetAnnotations()
			delete(annotations, bmpAnnotationPaused)
			bmp.SetAnnotations(annotations)

			log.Info(fmt.Sprintf("deleting baremetalpool %s on local cluster", bmpName))
			err = dynClientLocal.Resource(gvrbmp).Namespace(bmpNamespace).Delete(ctx, bmpName, metav1.DeleteOptions{})
			if err != nil {
				log.Error(err, "error while deleting baremetalpool resource on local cluster")
				return err
			}

			bmp.SetResourceVersion("")
			log.Info(fmt.Sprintf("creating baremetalpool %s on target cluster", bmpName))
			_, err = dynClientTarget.Resource(gvrbmp).Namespace(bmpNamespace).Create(ctx, bmp, metav1.CreateOptions{})
			if err != nil {
				log.Error(err, "error while creating baremetalpool resource on target cluster")
				return err
			}

		} else {
			if !k8serrors.IsNotFound(err) {
				log.Error(err, "error while getting baremetalpool on local cluster")
				return err
			} else {
				log.Error(err, fmt.Sprintf("baremetalpool %s missing in local cluster", bmpName))
				return err
			}
		}
	}
	return nil
}

// Move network resources listed in the clusterdef spec to the target cluster
func (r *ClusterDefReconciler) moveNetworkResources(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	log logr.Logger) error {

	restConfigLocal, _ := rest.InClusterConfig()
	dynClientLocal, err := dynamic.NewForConfig(restConfigLocal)
	if err != nil {
		log.Error(err, "Problem while creating client for local cluster.")
		return err
	}

	clientConfig := clientcmd.NewDefaultClientConfig(
		*cache.Kubeconfig,
		&clientcmd.ConfigOverrides{},
	)

	restConfigTarget, _ := clientConfig.ClientConfig()

	dynClientTarget, err := dynamic.NewForConfig(restConfigTarget)
	if err != nil {
		log.Error(err, "Problem while creating client for target cluster.")
		return err
	}

	gvrnwk := schema.GroupVersionResource{
		Group:    "netpool.kanod.io",
		Version:  "v1",
		Resource: "networks",
	}

	gvrsecret := schema.GroupVersionResource{
		Group:    "",
		Version:  "v1",
		Resource: "secrets",
	}

	nwkNamespace := cache.ClusterNamespace

	for _, nwkName := range clusterDef.Spec.PivotInfo.NetworkList {
		network, err := dynClientLocal.Resource(gvrnwk).Namespace(nwkNamespace).Get(ctx, nwkName, metav1.GetOptions{})
		if err == nil {
			spec := network.Object["spec"].(map[string]interface{})
			credentialName := fmt.Sprintf("%v", spec["networkdefinitionCredentialName"])

			log.Info(fmt.Sprintf("Prepare moving network resource %s with networkdefinition secret %s", nwkName, credentialName))

			// get secret associated with the network on local cluster
			secret, err := dynClientLocal.Resource(gvrsecret).Namespace(nwkNamespace).Get(ctx, credentialName, metav1.GetOptions{})
			if err != nil {
				log.Error(err, "error while getting networkdefinition secret on local cluster")
				return err
			}

			// create secret associated with the network on target cluster
			log.Info(fmt.Sprintf("creating networkdefinition secret %s on target cluster", credentialName))
			secret.SetResourceVersion("")
			_, err = dynClientTarget.Resource(gvrsecret).Namespace(nwkNamespace).Create(ctx, secret, metav1.CreateOptions{})
			if err != nil {
				log.Error(err, "error while creating networkdefinition secret on target cluster")
				return err
			}

			log.Info(fmt.Sprintf("deleting network %s on local cluster", nwkName))
			err = dynClientLocal.Resource(gvrnwk).Namespace(nwkNamespace).Delete(ctx, nwkName, metav1.DeleteOptions{})
			if err != nil {
				log.Error(err, "error while deleting baremetalpool resource on local cluster")
				return err
			}

			network.SetResourceVersion("")
			log.Info(fmt.Sprintf("creating network %s on target cluster", nwkName))
			_, err = dynClientTarget.Resource(gvrnwk).Namespace(nwkNamespace).Create(ctx, network, metav1.CreateOptions{})
			if err != nil {
				log.Error(err, "error while creating network resource on target cluster")
				return err
			}

		} else {
			if !k8serrors.IsNotFound(err) {
				log.Error(err, "error while getting network on local cluster")
				return err
			} else {
				log.Error(err, fmt.Sprintf("network %s missing in local cluster", nwkName))
				return err
			}
		}
	}
	return nil
}

// unpause baremetalpool resources on target cluster
func (r *ClusterDefReconciler) unpauseBaremetalpoolResources(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	log logr.Logger) (error, bool) {

	restConfigLocal, _ := rest.InClusterConfig()
	dynClientLocal, err := dynamic.NewForConfig(restConfigLocal)
	if err != nil {
		log.Error(err, "Problem while creating client for local cluster.")
		return err, false
	}

	clientConfig := clientcmd.NewDefaultClientConfig(
		*cache.Kubeconfig,
		&clientcmd.ConfigOverrides{},
	)

	restConfigTarget, _ := clientConfig.ClientConfig()

	dynClientTarget, err := dynamic.NewForConfig(restConfigTarget)
	if err != nil {
		log.Error(err, "Problem while creating client for target cluster.")
		return err, false
	}

	gvrbmp := schema.GroupVersionResource{
		Group:    "bmp.kanod.io",
		Version:  "v1",
		Resource: "baremetalpools",
	}

	bmpNamespace := cache.ClusterNamespace

	for _, bmpName := range clusterDef.Spec.PivotInfo.BaremetalpoolList {
		// check if baremetalpool exists on local cluster
		_, err := dynClientLocal.Resource(gvrbmp).Namespace(bmpNamespace).Get(ctx, bmpName, metav1.GetOptions{})
		if err != nil {
			if !k8serrors.IsNotFound(err) {
				log.Error(err, "error while getting baremetalpool on local cluster")
				return err, false
			} else {
				bmpTarget, err := dynClientTarget.Resource(gvrbmp).Namespace(bmpNamespace).Get(ctx, bmpName, metav1.GetOptions{})
				if err != nil {
					log.Error(err, "error while getting baremetalpool on target cluster")
					return err, false
				}
				annotations := bmpTarget.GetAnnotations()
				delete(annotations, bmpAnnotationPaused)
				bmpTarget.SetAnnotations(annotations)

				_, err = dynClientTarget.Resource(gvrbmp).Namespace(bmpNamespace).Update(ctx, bmpTarget, metav1.UpdateOptions{})
				if err != nil {
					log.Error(err, "error while updating baremetalpool finalizer on target cluster")
					return err, false
				}
			}
		} else {
			return nil, false
		}
	}
	return nil, true
}

// Add pause annotation on baremetalpool on local cluster
func (r *ClusterDefReconciler) pauseBaremetalpool(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	log logr.Logger) error {

	restConfigLocal, _ := rest.InClusterConfig()
	dynClientLocal, err := dynamic.NewForConfig(restConfigLocal)
	if err != nil {
		log.Error(err, "Problem while creating client for local cluster.")
		return err
	}

	gvrbmp := schema.GroupVersionResource{
		Group:    "bmp.kanod.io",
		Version:  "v1",
		Resource: "baremetalpools",
	}

	bmpNamespace := cache.ClusterNamespace

	// annotationPatch := fmt.Sprintf(`[{"op":"add","path":"/metadata/annotations","value":{"%s":""} }]`, bmpAnnotationPaused)

	for _, bmpName := range clusterDef.Spec.PivotInfo.BaremetalpoolList {
		bmp, err := dynClientLocal.Resource(gvrbmp).Namespace(bmpNamespace).Get(ctx, bmpName, metav1.GetOptions{})
		if err == nil {
			annotations := bmp.GetAnnotations()
			annotations[bmpAnnotationPaused] = ""
			bmp.SetAnnotations(annotations)

			// remove baremetalpool finalizer in local cluster
			finalizers := bmp.GetFinalizers()
			var newfinalizers []string
			for _, finalizerItem := range finalizers {
				if finalizerItem != baremetalpoolFinalizer {
					newfinalizers = append(newfinalizers, finalizerItem)
				}
			}
			bmp.SetFinalizers(newfinalizers)

			_, err = dynClientLocal.Resource(gvrbmp).Namespace(bmpNamespace).Update(ctx, bmp, metav1.UpdateOptions{})
			if err != nil {
				log.Error(err, "error while updating baremetalpool finalizer on local cluster")
				return err
			}
		} else {
			log.Error(err, fmt.Sprintf("error while getting baremetalpool %s  in local cluster", bmpName))
			return err
		}
	}

	return nil
}

// Add pause annotation on network operator resources on local cluster
func (r *ClusterDefReconciler) pauseNetwork(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	log logr.Logger) error {

	restConfigLocal, _ := rest.InClusterConfig()
	dynClientLocal, err := dynamic.NewForConfig(restConfigLocal)
	if err != nil {
		log.Error(err, "Problem while creating client for local cluster.")
		return err
	}

	gvrnetwork := schema.GroupVersionResource{
		Group:    "netpool.kanod.io",
		Version:  "v1",
		Resource: "networks",
	}

	networkNamespace := cache.ClusterNamespace

	for _, networkName := range clusterDef.Spec.PivotInfo.NetworkList {
		network, err := dynClientLocal.Resource(gvrnetwork).Namespace(networkNamespace).Get(ctx, networkName, metav1.GetOptions{})
		if err == nil {
			annotations := network.GetAnnotations()
			annotations[networkAnnotationPaused] = ""
			network.SetAnnotations(annotations)

			// remove network finalizer from network in local cluster
			finalizers := network.GetFinalizers()
			var newfinalizers []string
			for _, finalizerItem := range finalizers {
				if finalizerItem != networkFinalizer {
					newfinalizers = append(newfinalizers, finalizerItem)
				}
			}

			network.SetFinalizers(newfinalizers)

			_, err = dynClientLocal.Resource(gvrnetwork).Namespace(networkNamespace).Update(ctx, network, metav1.UpdateOptions{})
			if err != nil {
				log.Error(err, "error while updating network on local cluster")
				return err
			}
		} else {
			log.Error(err, fmt.Sprintf("error while getting network %s  in local cluster", networkName))
			return err
		}
	}

	return nil
}

// unpause network operator resources on target cluster
func (r *ClusterDefReconciler) unpauseNetworkResources(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	log logr.Logger) (error, bool) {

	restConfigLocal, _ := rest.InClusterConfig()
	dynClientLocal, err := dynamic.NewForConfig(restConfigLocal)
	if err != nil {
		log.Error(err, "Problem while creating client for local cluster.")
		return err, false
	}

	clientConfig := clientcmd.NewDefaultClientConfig(
		*cache.Kubeconfig,
		&clientcmd.ConfigOverrides{},
	)

	restConfigTarget, _ := clientConfig.ClientConfig()

	dynClientTarget, err := dynamic.NewForConfig(restConfigTarget)
	if err != nil {
		log.Error(err, "Problem while creating client for target cluster.")
		return err, false
	}

	gvrnetwork := schema.GroupVersionResource{
		Group:    "netpool.kanod.io",
		Version:  "v1",
		Resource: "networks",
	}

	networkNamespace := cache.ClusterNamespace

	for _, networkName := range clusterDef.Spec.PivotInfo.NetworkList {
		// check if network exists on local cluster
		_, err := dynClientLocal.Resource(gvrnetwork).Namespace(networkNamespace).Get(ctx, networkName, metav1.GetOptions{})
		if err != nil {
			if !k8serrors.IsNotFound(err) {
				log.Error(err, "error while getting network on local cluster")
				return err, false
			} else {
				networkTarget, err := dynClientTarget.Resource(gvrnetwork).Namespace(networkNamespace).Get(ctx, networkName, metav1.GetOptions{})
				if err != nil {
					log.Error(err, "error while getting network on target cluster")
					return err, false
				}
				annotations := networkTarget.GetAnnotations()
				delete(annotations, networkAnnotationPaused)
				networkTarget.SetAnnotations(annotations)

				_, err = dynClientTarget.Resource(gvrnetwork).Namespace(networkNamespace).Update(ctx, networkTarget, metav1.UpdateOptions{})
				if err != nil {
					log.Error(err, "error while updating network on target cluster")
					return err, false
				}
			}
		} else {
			return nil, false
		}
	}
	return nil, true
}

// Move kanod custom resource
func (r *ClusterDefReconciler) moveKanodResources(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	log logr.Logger) error {

	restConfigLocal, _ := rest.InClusterConfig()
	dynClientLocal, err := dynamic.NewForConfig(restConfigLocal)
	if err != nil {
		log.Error(err, "Problem while creating client for local cluster.")
		return err
	}

	clientConfig := clientcmd.NewDefaultClientConfig(
		*cache.Kubeconfig,
		&clientcmd.ConfigOverrides{},
	)
	restConfigTarget, _ := clientConfig.ClientConfig()

	dynClientTarget, err := dynamic.NewForConfig(restConfigTarget)
	if err != nil {
		log.Error(err, "Problem while creating client for target cluster.")
		return err
	}

	gvr := schema.GroupVersionResource{
		Group:    "config.kanod.io",
		Version:  "v1",
		Resource: "kanods",
	}

	gvrcm := schema.GroupVersionResource{
		Group:    "",
		Version:  "v1",
		Resource: "configmaps",
	}

	// Get kanod custom resource on localcluster
	kanodCrName := clusterDef.Spec.PivotInfo.KanodName
	kanodCrNamespace := clusterDef.Spec.PivotInfo.KanodNamespace
	kanodCustomResource, err := dynClientLocal.Resource(gvr).Namespace(kanodCrNamespace).Get(ctx, kanodCrName, metav1.GetOptions{})

	if err != nil {
		if !k8serrors.IsNotFound(err) {
			log.Error(err, "error while getting kanod custom resource on local cluster")
			return err
		} else {
			log.Error(err, fmt.Sprintf("kanod custom resource %s missing in local cluster", kanodCrName))
			return err
		}
	}

	// Get kanod configmap name in custom resource
	kanodConfigName, ok, err := unstructured.NestedString(kanodCustomResource.Object, "spec", "configName")
	if err != nil || !ok {
		log.Error(err, "failed to get configName field in kanod custom resource")
		return err
	}

	// Get kanod configmap in local cluster
	kanodConfig, err := dynClientLocal.Resource(gvrcm).Namespace(clusterDef.Spec.PivotInfo.KanodNamespace).Get(ctx, kanodConfigName, metav1.GetOptions{})
	if err != nil {
		if !k8serrors.IsNotFound(err) {
			log.Error(err, "error while getting kanod config configmap on local cluster")
			return err
		} else {
			log.Error(err, fmt.Sprintf("kanod config configmap %s missing in local cluster", kanodConfigName))
			return err
		}
	}

	// Create kanod configmap in target cluster
	kanodConfig.SetResourceVersion("")

	_, err = dynClientTarget.Resource(gvrcm).Namespace(clusterDef.Spec.PivotInfo.KanodNamespace).Get(ctx, kanodConfigName, metav1.GetOptions{})

	if err != nil {
		if !k8serrors.IsNotFound(err) {
			log.Error(err, "error while getting kanod configmap in target cluster")
			return err
		} else {
			log.Info(fmt.Sprintf("creating kanod configmap %s in target cluster", kanodConfigName))
			_, err = dynClientTarget.Resource(gvrcm).Namespace(clusterDef.Spec.PivotInfo.KanodNamespace).Create(ctx, kanodConfig, metav1.CreateOptions{})
			if err != nil {
				log.Error(err, fmt.Sprintf("error while creating kanod config configmap %s on target cluster", kanodConfigName))
				return err
			}
		}
	} else {
		log.Info(fmt.Sprintf("kanod configmap %s exists in target cluster\n", kanodConfigName))
	}

	// Create kanod custom resouce in target cluster
	unstructured.RemoveNestedField(kanodCustomResource.Object, "spec", "argocd")
	unstructured.RemoveNestedField(kanodCustomResource.Object, "spec", "ingress")

	kanodCustomResource.SetResourceVersion("")
	annotations := kanodCustomResource.GetAnnotations()
	delete(annotations, "operator.kanod.io/progression")
	kanodCustomResource.SetAnnotations(annotations)

	_, err = dynClientTarget.Resource(gvr).Namespace(kanodCrNamespace).Get(ctx, kanodCrName, metav1.GetOptions{})
	if err != nil {
		if !k8serrors.IsNotFound(err) {
			log.Error(err, "error while kanod custom resource in target cluster")
			return err
		} else {
			log.Info(fmt.Sprintf("creating kanod custom resource %s in target cluster", kanodCrName))
			_, err = dynClientTarget.Resource(gvr).Namespace(kanodCrNamespace).Create(ctx, kanodCustomResource, metav1.CreateOptions{})
			if err != nil {
				log.Error(err, fmt.Sprintf("error while creating kanod custom resource %s on target cluster", kanodCrName))
				return err
			}
		}
	} else {
		log.Info(fmt.Sprintf("kanod custom resource %s exists in target cluster\n", kanodCrName))
	}

	return nil
}

func (r *ClusterDefReconciler) isStackDeployed(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	log logr.Logger,
) (error, bool) {
	clientConfig := clientcmd.NewDefaultClientConfig(
		*cache.Kubeconfig,
		&clientcmd.ConfigOverrides{},
	)
	restConfigTarget, _ := clientConfig.ClientConfig()

	dynClientTarget, err := dynamic.NewForConfig(restConfigTarget)
	if err != nil {
		log.Error(err, "Problem while creating client for target cluster.")
		return err, false
	}

	gvr := schema.GroupVersionResource{
		Group:    "config.kanod.io",
		Version:  "v1",
		Resource: "kanods",
	}

	kanodCrName := clusterDef.Spec.PivotInfo.KanodName
	kanodCrNamespace := clusterDef.Spec.PivotInfo.KanodNamespace
	kanodCustomResource, err := dynClientTarget.Resource(gvr).Namespace(kanodCrNamespace).Get(ctx, kanodCrName, metav1.GetOptions{})

	if err != nil {
		if !k8serrors.IsNotFound(err) {
			log.Error(err, "error while getting kanod custom resource on target cluster")
			return err, false
		} else {
			log.Error(err, fmt.Sprintf("kanod custom resource %s missing in target cluster", kanodCrName))
			return err, false
		}
	}

	value, ok, err := unstructured.NestedSlice(kanodCustomResource.Object, "status", "conditions")
	if err != nil {
		log.Error(err, "error while getting conditions in kanod custom resource on target cluster")
		return err, false
	}

	if ok {
		for _, conditionsList := range value {
			condition := conditionsList.(map[string]interface{})
			if condition["type"] == "KanodStackReady" && condition["status"] == "True" {
				return nil, true
			}
		}
	} else {
		log.Info("conditions missing in kanod custom resource on target cluster")
		return nil, false
	}

	return nil, false
}

// Install kanod operator on target cluster
func (r *ClusterDefReconciler) installKanodOperator(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	log logr.Logger) error {

	restConfigLocal, _ := rest.InClusterConfig()
	dynClientLocal, err := dynamic.NewForConfig(restConfigLocal)
	if err != nil {
		log.Error(err, "Problem while creating client for local cluster.")
		return err
	}

	gvr := schema.GroupVersionResource{
		Group:    "config.kanod.io",
		Version:  "v1",
		Resource: "kanods",
	}

	kanodCrName := clusterDef.Spec.PivotInfo.KanodName
	kanodCrNamespace := clusterDef.Spec.PivotInfo.KanodNamespace
	kanodCustomResource, err := dynClientLocal.Resource(gvr).Namespace(kanodCrNamespace).Get(ctx, kanodCrName, metav1.GetOptions{})

	if err != nil {
		if !k8serrors.IsNotFound(err) {
			log.Error(err, "error while getting kanod custom resource on local cluster")
			return err
		} else {
			log.Error(err, fmt.Sprintf("kanod custom resource %s missing in local cluster", kanodCrName))
			return err
		}
	}

	kanodConfigName, ok, err := unstructured.NestedString(kanodCustomResource.Object, "spec", "configName")
	if err != nil || !ok {
		log.Error(err, "failed to get configName field in kanod custom resource")
		return err
	}

	var config = &corev1.ConfigMap{}
	cmapMeta := client.ObjectKey{
		Name:      kanodConfigName,
		Namespace: kanodCrNamespace,
	}
	err = r.Client.Get(ctx, cmapMeta, config)
	if err != nil {
		r.Log.Error(err, "Cannot find the base config", "configName", kanodConfigName)
		return err
	}
	repository, ok := config.Data["repository"]
	if !ok {
		r.Log.Error(err, "No repository defined in kanod config", "configName", kanodConfigName)
		return err
	}

	registry, ok := config.Data["registry"]
	if !ok {
		r.Log.Error(err, "No registry defined in kanod config", "configName", kanodConfigName)
		return err
	}

	var env = &Env{Env: make(map[string]string)}
	env.Setenv("NEXUS", repository)
	env.Setenv("NEXUS_REGISTRY", registry)

	err = r.ApplyFile(ctx, env, r.Config.KanodOperatorCrd, cache)
	if err != nil {
		r.Log.Error(err, "Cannot perform apply kanod crd.", "url", r.Config.KanodOperatorCrd)
		return err
	}

	return nil
}

func (r *ClusterDefReconciler) isKanodDeployed(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	log logr.Logger,
) (error, bool) {
	clientConfig := clientcmd.NewDefaultClientConfig(
		*cache.Kubeconfig,
		&clientcmd.ConfigOverrides{},
	)
	restConfigTarget, _ := clientConfig.ClientConfig()

	dynClientTarget, err := dynamic.NewForConfig(restConfigTarget)
	if err != nil {
		log.Error(err, "Problem while creating client for target cluster.")
		return err, false
	}

	gvr := schema.GroupVersionResource{
		Group:    "apps",
		Version:  "v1",
		Resource: "deployments",
	}

	kanodDeploymentName := kanodDeploymentName
	kanodDeploymentNamespace := kanodDeploymentNamespace

	kanodDeployment, err := dynClientTarget.Resource(gvr).Namespace(kanodDeploymentNamespace).Get(ctx, kanodDeploymentName, metav1.GetOptions{})

	if err != nil {
		if !k8serrors.IsNotFound(err) {
			log.Error(err, "error while getting kanod deployment on target cluster")
			return err, false
		} else {
			log.Error(err, fmt.Sprintf("kanod deployment %s missing in target cluster", kanodDeploymentName))
			return err, false
		}
	}

	value, ok, err := unstructured.NestedSlice(kanodDeployment.Object, "status", "conditions")
	if err != nil {
		log.Error(err, "error while getting conditions in kanod deployment on target cluster")
		return err, false
	}

	if ok {
		for _, conditionsList := range value {
			condition := conditionsList.(map[string]interface{})
			if condition["type"] == string(appsv1.DeploymentAvailable) && condition["status"] == string(corev1.ConditionTrue) {
				return nil, true
			}
		}
	} else {
		log.Info("conditions missing in kanod deployment on target cluster")
		return nil, false
	}

	return nil, false
}

func (r *ClusterDefReconciler) createNamespace(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	nsName string,
	log logr.Logger,
) error {

	clientConfig := clientcmd.NewDefaultClientConfig(
		*cache.Kubeconfig,
		&clientcmd.ConfigOverrides{},
	)
	restConfigTarget, _ := clientConfig.ClientConfig()

	dynClientTarget, err := dynamic.NewForConfig(restConfigTarget)
	if err != nil {
		log.Error(err, "Problem while creating client for target cluster.")
		return err
	}

	gvrns := schema.GroupVersionResource{
		Group:    "",
		Version:  "v1",
		Resource: "namespaces",
	}

	nsSpec := &unstructured.Unstructured{
		Object: map[string]interface{}{
			"apiVersion": "v1",
			"kind":       "Namespace",
			"metadata": map[string]interface{}{
				"name": nsName,
			},
		},
	}

	_, err = dynClientTarget.Resource(gvrns).Get(ctx, nsName, metav1.GetOptions{})

	if err != nil {
		if !k8serrors.IsNotFound(err) {
			log.Error(err, "error while getting namespace on target cluster")
			return err
		} else {
			log.Info(fmt.Sprintf("creating namespace %s on target cluster", nsName))
			_, err = dynClientTarget.Resource(gvrns).Create(ctx, nsSpec, metav1.CreateOptions{})
			if err != nil {
				log.Error(err, fmt.Sprintf("error while creating namespace %s on target cluster", nsName))
				return err
			}
		}
	} else {
		log.Info(fmt.Sprintf("namespace %s exists in target cluster\n", nsName))
	}

	return nil
}

func (r *ClusterDefReconciler) pauseCapi(ctx context.Context, clusterDef *gitopsv1.ClusterDef, log logr.Logger) error {
	pausePatch := `{"spec":{"paused": true}}`
	err := r.Client.Patch(
		ctx,
		&capi.Cluster{
			ObjectMeta: metav1.ObjectMeta{
				Name:      clusterDef.Name,
				Namespace: clusterDef.Namespace,
			},
		},
		client.RawPatch(types.MergePatchType, []byte(pausePatch)),
	)
	if err != nil {
		r.Log.Error(err, "Cannot patch clusterapi.")
	}

	return err
}

func (r *ClusterDefReconciler) scaleLocalIronicDeployment(ctx context.Context, replicasValue int32, log logr.Logger) error {
	patch := fmt.Sprintf(`{"spec":{"replicas": %d}}`, replicasValue)

	err := r.Client.Patch(
		ctx,
		&appsv1.Deployment{
			ObjectMeta: metav1.ObjectMeta{
				Name:      ironicDeploymentName,
				Namespace: ironicDeploymentNamespace,
			},
		},
		client.RawPatch(types.MergePatchType, []byte(patch)),
	)
	if err != nil {
		r.Log.Error(err, "Cannot scale baremetal-operator-ironic deployment.")
	}

	return err
}

func (r *ClusterDefReconciler) isIronicDeploymentReady(
	ctx context.Context,
	log logr.Logger,
) (error, bool) {
	var ironicDeployment appsv1.Deployment

	err := r.Client.Get(
		ctx,
		types.NamespacedName{
			Name:      ironicDeploymentName,
			Namespace: ironicDeploymentNamespace,
		},
		&ironicDeployment,
	)

	if err != nil {
		log.Error(err, "Cannot get ironic deployment on local cluster.")
		return err, false
	}

	for _, condition := range ironicDeployment.Status.Conditions {
		if condition.Type == appsv1.DeploymentAvailable && condition.Status == corev1.ConditionTrue {
			return nil, true
		}
	}

	return nil, false
}
