/*
Copyright 2020-22 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"

	"github.com/go-logr/logr"
	"github.com/pkg/errors"
	gitopsv1 "gitlab.com/Orange-OpenSource/kanod/cluster-def/api/v1alpha1"
	argocd "gitlab.com/Orange-OpenSource/kanod/cluster-def/thirdparty/argocd/apis/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
)

// makeApplication create the argocd application associated to the clusterDef
func (r *ClusterDefReconciler) makeApplication(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	destServer string,
	log logr.Logger,
) error {
	var name = fmt.Sprintf("cdef-%s", clusterDef.Name)
	var app argocd.Application
	app.ObjectMeta = metav1.ObjectMeta{
		Name:      name,
		Namespace: r.Config.ArgoCDNamespace}
	_, err := controllerutil.CreateOrUpdate(
		ctx, r.Client, &app,
		func() error {
			spec := &app.Spec
			source := &spec.Source
			dest := &spec.Destination
			source.RepoURL = clusterDef.Spec.Source.Repository
			source.Path = "."
			if clusterDef.Spec.Source.Path != "" {
				source.Path = clusterDef.Spec.Source.Path
			}
			source.Plugin = &argocd.ApplicationSourcePlugin{
				Env: []*argocd.EnvEntry{
					{
						Name:  "INFRA_CONFIGMAP",
						Value: clusterDef.Spec.ConfigurationName,
					},
					{
						Name:  "INFRA_NAMESPACE",
						Value: r.Config.InfraNamespace,
					},
					{
						Name:  "CDEF_NAMESPACE",
						Value: clusterDef.Namespace,
					},
					{
						Name:  "TARGET_NAMESPACE",
						Value: clusterDef.Namespace,
					},
					{
						Name:  "CIDATA_CONFIGMAP",
						Value: clusterDef.Spec.CidataName,
					},
					{
						Name:  "CLUSTER_NAME",
						Value: clusterDef.Name,
					},
					{
						Name:  "CLUSTER_CONFIG_FILE",
						Value: r.Config.ClusterFileName,
					},
					{
						Name:  "PLUGIN_MODE",
						Value: "updater",
					},
				},
			}
			dest.Namespace = clusterDef.Namespace
			dest.Server = destServer
			spec.IgnoreDifferences = []argocd.ResourceIgnoreDifferences{
				{
					Group: "infrastructure.cluster.x-k8s.io",
					JSONPointers: []string{
						"/spec/noCloudProvider",
					},
					Kind: "Metal3Cluster",
				},
			}
			spec.Project = "default"
			spec.SyncPolicy = &argocd.SyncPolicy{
				Automated: &argocd.SyncPolicyAutomated{SelfHeal: true},
			}
			return nil
		},
	)
	return err
}

// makeCredentials create a credential secret for the repository
func (r *ClusterDefReconciler) makeCredentials(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	log logr.Logger,
) error {
	// No credentials defined.
	// TODO: We should probably remove the secret if it existed
	if clusterDef.Spec.CredentialName == "" {
		return nil
	}
	name := fmt.Sprintf("cdef-%s", clusterDef.Name)
	var sec corev1.Secret
	sec.ObjectMeta = metav1.ObjectMeta{
		Name: name, Namespace: r.Config.ArgoCDNamespace,
	}
	_, err := controllerutil.CreateOrUpdate(
		ctx, r.Client, &sec,
		func() error {
			if sec.ObjectMeta.Labels == nil {
				sec.ObjectMeta.Labels = make(map[string]string)
			}
			sec.ObjectMeta.Labels["argocd.argoproj.io/secret-type"] = "repository"
			var ref = &corev1.Secret{}
			refMeta := client.ObjectKey{
				Name:      clusterDef.Spec.CredentialName,
				Namespace: clusterDef.Namespace,
			}
			if err := r.Client.Get(ctx, refMeta, ref); err == nil {
				if sec.Data == nil {
					sec.Data = make(map[string][]byte)
				}
				for key, val := range ref.Data {
					sec.Data[key] = val
				}
				sec.Data["url"] = []byte(clusterDef.Spec.Source.Repository)
				return nil
			} else {
				return errors.Wrap(err, "Cannot access referenced secret")
			}
		},
	)
	return errors.Wrap(err, "Credential creation failed")
}
