Overview
========
ClusterDef is the custom resource describing a git repository containing a
cluster definition in Kanod. This definition must contain a synthetic
description of the cluster in terms of control-plane and workers nodes templates.

The CRD contains a link to a secret providing the credentials necessary to
access the cluster. The git repository is under the control of a cluster
administrator who can be viewed as a customer of the Kanod instance.

The templates are defined by the Infrastructure Manager (who is the Kanod
administrator). They are contained in a ConfigMap referenced by the custom
resource.

The role of the operator is:

* to synthesize Metal3 resources from the descriptions
  provided by the cluster administrator and the infrastructure manager using
  the kanod-updater.
* to synthesize resources to deploy on the target cluster as soon as it has
  started.
* to ensure that evolutions are propagated to the target cluster.

ClusterDef Deletion
===================
ArgoCD implementers have already noticed the risk of deleting a complex deployment
because the application object had been removed. Therefore only the management
of the application is stopped when the resource is removed unless the
finalizer ``resources-finalizer.argocd.argoproj.io`` has been added to
the application CRD. In the later case, a cascaded deletion occurs and all the
resources owned by the application are removed.

ClusterDef follows the same principle. To actually destroy the cluster associated
to a ClusterDef CRD, the resource must be annotated with the
``resources-finalizer.gitops.kanod.io`` finalizer.

Implementation
==============
The operator just creates an argocd application with the right plugin that
will transform the synthetic description in a correct cluster api description.

The name of the configmap containing the infrastructure template is given to
the updater through an environment variable declared in the application.


