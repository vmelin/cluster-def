# ClusterDef

`ClusterDef` is a new kind for a new custom resource definition that describes a end-user cluster definition.
It contains just a git location and the name of a secret in the same namespace where the credentials are stored.

This project implements both the CRD definition and the controller using kubebuilder.

# API

Here is an example project configuration:

```
apiVersion: gitops.kanod.io/v1alpha1
kind: ClusterDef
metadata:
  name: cluster-name
  namespace: namespace
spec:
  source:
    repository: ... url ...
    branch: master
  credentialName: project-creds
---
apiVersion: v1
kind: Secret
metadata:
  name: project-creds
  namespace: argocd
type: Opaque
data:
  username: ... base 64 encoded ...
  password: ... base 64 encoded ...
```

## Credentials for git
There are two options:
* for https URL, the secret must contain two fields: username and password.
* for git ssh URL, the secret must contain a single ssh field whose value is the
  private ssh key necessary to access the repository.

# Configuration of the controller
The controller is configured through a configmap installed in its namespace: `clusterdef-cm`. It defines the
value for the following entries:
* `SNAPSHOT_GIT` The git of the cluster snapshots. It is the controller that controls the branch allocated.
* `SNAPSHOT_CREDENTIALS` The secret name containing the credentials
* `REGISTRY` The registry for container imags.
* `HTTP_PROXY` HTTP proxy (used by the updater)
* `HTTPS_PROXY` HTTPS proxy (used by the updater)
* `NO_PROXY` NO proxy variable (used by the updater)

The content of the secret pointed by `SNAPSHOT_CREDENTIALS` depends on 
the way the updater is used:
* for https git url, it must contain entries `SNAPSHOT_GIT_USERNAME` and
  `SNAPSHOT_GIT_PASSWORD`
* for ssh git url, it must contain an entry `SNAPSHOT_GIT_SSHKEY`.
* when used with Vault, the secret must also contain the following fields:
    * `VAULT_URL` the URL used to access the Vault service
    * `VAULT_CA` root certificate to authenticate the connection to Vault.
    * `VAULT_SECRET` The secret-id for the updater app-role (the role id is
       provided through the infra project). This secret is usually extracted
       from Vault by the kanod-configure run on the life-cycle manager.
