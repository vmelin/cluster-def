/*
Copyright 2020 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	"fmt"

	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
	"sigs.k8s.io/controller-runtime/pkg/webhook"
)

// log is for logging in this package.
var clusterdeflog = logf.Log.WithName("clusterdef-resource")

func (r *ClusterDef) SetupWebhookWithManager(mgr ctrl.Manager) error {
	return ctrl.NewWebhookManagedBy(mgr).
		For(r).
		Complete()
}

//+kubebuilder:webhook:path=/mutate-gitops-kanod-io-v1alpha1-clusterdef,mutating=true,failurePolicy=fail,groups=gitops.kanod.io,resources=clusterdefs,verbs=create;update,versions=v1alpha1,name=mclusterdef.kb.io,sideEffects=None,admissionReviewVersions=v1beta1

var _ webhook.Defaulter = &ClusterDef{}

// Default implements webhook.Defaulter so a webhook will be registered for the type
func (r *ClusterDef) Default() {
	clusterdeflog.Info("default", "name", r.Name)

}

//+kubebuilder:webhook:verbs=create;update,path=/validate-gitops-kanod-io-v1alpha1-clusterdef,mutating=false,failurePolicy=fail,groups=gitops.kanod.io,resources=clusterdefs,versions=v1alpha1,name=vclusterdef.kb.io,sideEffects=None,admissionReviewVersions=v1beta1

var _ webhook.Validator = &ClusterDef{}

// ValidateCreate implements webhook.Validator so a webhook will be registered for the type
func (r *ClusterDef) ValidateCreate() error {
	clusterdeflog.Info("validate create", "name", r.Name)
	if r.Spec.PivotInfo.Pivot {
		err := fmt.Errorf("Spec.PivotInfo.Pivot must be false when creating ClusterDef resource")
		return err
	}
	return nil
}

// ValidateUpdate implements webhook.Validator so a webhook will be registered for the type
func (r *ClusterDef) ValidateUpdate(old runtime.Object) error {
	clusterdeflog.Info("validate update", "name", r.Name)

	if !r.Spec.PivotInfo.Pivot && r.Status.PivotStatus == ClusterPivoted {
		err := fmt.Errorf("Spec.Pivot value cannot be changed when cluster is pivoted")
		return err
	}
	return nil
}

// ValidateDelete implements webhook.Validator so a webhook will be registered for the type
func (r *ClusterDef) ValidateDelete() error {
	clusterdeflog.Info("validate delete", "name", r.Name)

	return nil
}
