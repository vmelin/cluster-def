/*
Copyright 2020 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// ClusterDefSpec defines the desired state of ClusterDef
type ClusterDefSpec struct {

	// Source is the location of the cluster definition.
	Source GitLocation `json:"source"`
	// CredentialName is the user credentials for accessing git repository
	// It must be in a secret in the same namespace
	CredentialName string `json:"credentialName"`
	// ConfigurationName is the configuration supplied by the infrastructure
	// administrator to render the specification of the cluster definition
	// It is a configmap in the same namespace
	ConfigurationName string `json:"configurationName"`
	// CidataName is the name of the configmap that customize the cluster
	// cloud-init datas. This has precedence over all other sources of
	// cloud-init data.
	CidataName string `json:"cidataName,omitempty"`
	// PivotInfo indicates if the argocd application of the targer cluster .
	// must be deployed on the target cluster
	PivotInfo PivotInformation `json:"pivotInfo,omitempty"`
}

// Pivot defines informations nedded for pivoting
type PivotInformation struct {
	// Pivot indicates if the cluster pivoting process should be launched
	Pivot bool `json:"pivot"`
	// BaremetalpoolList contains the list of Baremetalpool
	// to be moved on the target cluster
	BaremetalpoolList []string `json:"baremetalpoolList,omitempty"`
	// NetworkList contains the list of Network
	// to be moved on the target cluster
	NetworkList []string `json:"networkList,omitempty"`
	// kanod custom resource name
	KanodName string `json:"kanodName"`
	// namespace of the kanod custom resource
	KanodNamespace string `json:"kanodNamespace"`
}

// GitLocation defines a complete path to a version of a folder on a
// git repository
type GitLocation struct {
	// Repository is the URL of the project hosting the files
	Repository string `json:"repository"`
	// Branch is the git branch followed by ClusterDef
	Branch string `json:"branch,omitempty"`
	// Path is the subpath of the folder hosting the configuration file.
	Path string `json:"path,omitempty"`
}

// ClusterDefStatus defines the observed state of ClusterDef
type ClusterDefStatus struct {
	// Status of pivot
	// +optional
	PivotStatus PivotState `json:"pivotstatus"`
	// Status of deployment
	// +optional
	Phase ClusterDefState `json:"phase"`
	// ControlPlaneVersion is the version supported by control plane
	// +optional
	ControlPlaneVersion string `json:"controlPlaneVersion"`
	// +optional
	// MachineVersions is a sorted array of different versions of the kubelet
	// deployed on machines
	MachineVersions []string `json:"machineVersions"`
}

// ClusterDefState describes the possible phase of the cluster definition
// +kubebuilder:validation:Enum=ApplicationCreated;ApplicationSynced;ClusterProvisionned;Ready;Failed
type ClusterDefState string

const (
	// ApplicationCreated means the resources have been created
	ApplicationCreated ClusterDefState = "ApplicationCreated"
	// ApplicationSynced means the application is synced
	ApplicationSynced ClusterDefState = "ApplicationSynced"
	// ClusterProvisionned means that it is possible to connect to the cluster
	ClusterProvisionned ClusterDefState = "ClusterProvisionned"
	// Ready means that nodes are ready.
	Ready ClusterDefState = "Ready"
	// Failed means that the application failed to deploy
	Failed ClusterDefState = "Failed"
)

// PivotState describes the possible phase of the cluster pivoting process
// +kubebuilder:validation:Enum=NotPivoted;TargetClusterInfoStored;KanodOperatorInstalled;KanodOperatorReady;KanodResourcesMoved;StackDeployed;ArgoAppDeleted;ResourcesLabeled;BaremetalpoolPaused;NetworkPaused;ResourcesPivoted;ResourcesUnlabeled;BmpResourcesPivoted;NetworkResourcesPivoted;BaremetalpoolUnPaused;NetworkUnPaused;IronicOnLocalClusterScaledToZero;IronicOnLocalClusterUndeployed;IronicOnLocalClusterScaledToOne;IronicOnLocalClusterRedeployed;ClusterPivoted
type PivotState string

const (
	// NotPivoted
	NotPivoted PivotState = "NotPivoted"
	// TargetClusterInfoStored
	TargetClusterInfoStored PivotState = "TargetClusterInfoStored"
	// KanodOperatorInstalled
	KanodOperatorInstalled PivotState = "KanodOperatorInstalled"
	// KanodOperatorReady
	KanodOperatorReady PivotState = "KanodOperatorReady"
	// KanodResourcesMoved
	KanodResourcesMoved PivotState = "KanodResourcesMoved"
	// StackDeployed
	StackDeployed PivotState = "StackDeployed"
	// ArgoAppDeleted
	ArgoAppDeleted PivotState = "ArgoAppDeleted"
	// ResourcesLabeled
	ResourcesLabeled PivotState = "ResourcesLabeled"
	// BaremetalpoolPaused
	BaremetalpoolPaused PivotState = "BaremetalpoolPaused"
	// BaremetalpoolPaused
	NetworkPaused PivotState = "NetworkPaused"
	// ResourcesPivoted
	ResourcesPivoted PivotState = "ResourcesPivoted"
	// ResourcesUnlabeled
	ResourcesUnlabeled PivotState = "ResourcesUnlabeled"
	// BmpResourcesPivoted
	BmpResourcesPivoted PivotState = "BmpResourcesPivoted"
	// NetworkResourcesPivoted
	NetworkResourcesPivoted PivotState = "NetworkResourcesPivoted"
	// BaremetalpoolUnPaused
	BaremetalpoolUnPaused PivotState = "BaremetalpoolUnPaused"
	// BaremetalpoolUnPaused
	NetworkUnPaused PivotState = "NetworkUnPaused"
	// IronicOnLocalClusterScaledToZero
	IronicOnLocalClusterScaledToZero PivotState = "IronicOnLocalClusterScaledToZero"
	// IronicOnLocalClusterUndeployed
	IronicOnLocalClusterUndeployed PivotState = "IronicOnLocalClusterUndeployed"
	// IronicOnLocalClusterScaledToOne
	IronicOnLocalClusterScaledToOne PivotState = "IronicOnLocalClusterScaledToOne"
	// IronicOnLocalClusterRedeployed
	IronicOnLocalClusterRedeployed PivotState = "IronicOnLocalClusterRedeployed"
	// ClusterPivoted
	ClusterPivoted PivotState = "ClusterPivoted"
)

// ClusterDef is the Schema for the clusterdefs API
// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// +kubebuilder:printcolumn:name="Phase",type=string,JSONPath=`.status.phase`
// +kubebuilder:printcolumn:name="Repository",type=string,JSONPath=`.spec.source.repository`
// +kubebuilder:printcolumn:name="Branch",type=string,JSONPath=`.spec.source.branch`,priority=10
// +kubebuilder:printcolumn:name="PivotStatus",type=string,JSONPath=`.status.pivotstatus`

type ClusterDef struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   ClusterDefSpec   `json:"spec,omitempty"`
	Status ClusterDefStatus `json:"status,omitempty"`
}

// ClusterDefList contains a list of ClusterDef
// +kubebuilder:object:root=true
type ClusterDefList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []ClusterDef `json:"items"`
}

func init() {
	SchemeBuilder.Register(&ClusterDef{}, &ClusterDefList{})
}
